import styled from 'styled-components';

const H4 = styled.h4`
  font-size: 1em;
  text-align: center;
`;

export default H4;

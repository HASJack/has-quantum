import React from 'react';
import { FormattedMessage } from 'react-intl';

import A from 'components/A';
import LocaleToggle from 'containers/LocaleToggle';
import Wrapper from './Wrapper';
import messages from './messages';

function Footer() {
  return (
    <Wrapper>
      <section>
        <LocaleToggle />
      </section>
      <section>

      </section>
      <section>
        <A href="https://halfasecond.com">half<span>a</span>second.com</A>
      </section>
    </Wrapper>
  );
}

export default Footer;

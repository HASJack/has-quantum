/**
 * A link to a certain page, an anchor tag
 */

import styled from 'styled-components';

const A = styled.a`
  color: #333;
  text-decoration: none;
  font-weight: bold;
  font-family: Helvetica, Arial, sans-serif;
  letter-spacing: 0.6px;

  &:hover {
    color: #6cc0e5;
  }

  span {
    color: #FF0000;
  }
`;

export default A;

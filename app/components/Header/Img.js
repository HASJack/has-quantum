import styled from 'styled-components';

import NormalImg from 'components/Img';

const Img = styled(NormalImg)`
  width: 50%;
  margin: 20px auto;
  display: block;
`;

export default Img;

/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';

import H1 from 'components/H1';
import messages from './messages';
import List from './List';
import ListItem from './ListItem';
import ListItemTitle from './ListItemTitle';

import phys from 'phys';

export default class Library extends React.Component { // eslint-disable-line react/prefer-stateless-function

  // Since state and props are static,
  // there's no need to re-render this component
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div>
        <Helmet
          title="Library"
          meta={[
            { name: 'description', content: 'Available Functions' },
          ]}
        />
        <H1>
          <FormattedMessage {...messages.header} />
        </H1>
        <div>
          {phys &&
            <List>
              { Object.keys(phys).map((i, index) =>
                <ListItem key={index}>
                  <ListItemTitle>{ `${i}` }</ListItemTitle>
                  {phys[i] &&
                    <List>
                      {Object.keys(phys[i]).map((q, index) =>
                        <ListItem key={q}>
                          <b>{ `${q}:` }</b> { `${q}: ${phys[i][q]}` }
                        </ListItem>
                      )}
                    </List>
                  }
                </ListItem>
              )}
            </List>
          }
         </div>
      </div>
    );
  }
}
